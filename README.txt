The ONE v1.4.1 - Readme
=======================

The ONE is a Opportunistic Network Environment simulator which provides a
powerful tool for generating mobility traces, running DTN messaging
simulations with different routing protocols, and visualizing both
simulations interactively in real-time and results after their completion.


This is my tesis using simulated anneling algorithm for 
opportunistic network in routing protocol bubblerap,prohet,simbet.